const request = require('request');
const beautify = require("json-beautify");
const fs = require('fs');

function Bot(){
    this.tag = tag => this.request({ tag })
    this.user = user => this.request({ user })
    this.request = ({ user, tag }) => {
        //mount config
        let config = {
                url: (
                    user ?
                    "https://www.instagram.com/" + user + "/"
                    : tag ?
                        "https://www.instagram.com/explore/tags/" + tag + "/"
                        :
                        null
                ),
                headers: {
                    'User-Agent': 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/60.0.3112.113 Safari/537.36'
                } 
            }
        //if theres a config.url return promise
        return config.url ? 
            new Promise((resolve, reject) => {

                //request html
                request(config, (error, response, html) => {
                    if (!error && response.statusCode === 200) {

                        //recieve html in string type, then search for window._sharedData
                        html = html.substring(html.indexOf('window._sharedData = {'));
                        //filter whole object and parse as json
                        html = html.substring(html.indexOf('{'), html.indexOf(';</script>'));
                        json = JSON.parse(html)

                        //pay promise
                        if(!error) {
                            resolve(this.response({json, user, tag}))
                        }
                        else reject(error)

                        //save file
                        // json = beautify(json, null, 2, 100);
                        // let file = user ? './logs/instaUSER'+ user +'.json' : './logs/instaTAG'+ tag +'.json'
                        // fs.writeFile(file, json, 'utf8', function(err) {
                        //     if (err) throw err;
                        // });
                    //handle error
                    } else if (error) {
                        console.log(error)
                    }
                })
            })
            //if !config.url return false
            : false
    }
    //format response object
    this.response = ({ json, user, tag }) => {
        
        //  ERROR
        if (!json) {
            return false
        //  USER
        } else if (json && user) {
            let posts = []
            let userData = json.entry_data.ProfilePage[0].graphql.user
            userData.edge_owner_to_timeline_media.edges
            .forEach((post, index) => {
                posts.push({
                    index: index,
                    img: post.node.thumbnail_src,
                    text: post.node.edge_media_to_caption.edges[0]
                        ?
                        post.node.edge_media_to_caption.edges[0].node.text : false
                })
            })
            return {
                instabot: {
                    action: "SCRAP_USER",
                    source: json
                },
                user: {
                    object: userData,
                    name: userData.full_name,
                    posts: posts
                }
            }
        //  TAG
        } else if (json && tag) {
            let posts = []
            let tagData = json.entry_data.TagPage[0].graphql.hashtag
            tagData.edge_hashtag_to_top_posts.edges
            .forEach((tag, index) => {
                posts.push({
                    index: index,
                    img: tag.node.thumbnail_src,
                    text: tag.node.edge_media_to_caption.edges[0]
                        ?
                        tag.node.edge_media_to_caption.edges[0].node.text : false
                })
            })
            return {
                instabot: {
                    action: "SCRAP_TAG",
                    source: json
                },
                tag: {
                    object: tagData,
                    name: tagData.name,
                    pic: tagData.profile_pic_url,
                    posts: posts
                }
            }
        }
    }
}

module.exports.scrap = new Bot();