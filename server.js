const express = require('express')
const next = require('next')
const instabot = require('./instabot.js')

const dev = process.env.NODE_ENV !== 'production'
const app = next({ dev })
const handle = app.getRequestHandler()
const PORT = process.env.PORT || 3000;

app.prepare()
.then(() => {
  const server = express()

  server.get('/u/:user', (req, res) => {
    instabot.scrap.user(req.params.user).then(result => {
      app.render(req, res, '/instauser', result)
    });
  })

  server.get('/t/:tag', (req, res) => {
    instabot.scrap.tag(req.params.tag).then(result => {
      app.render(req, res, '/instatag', result)
    }).catch(err => console.log(err));
  })

  server.get('*', (req, res) => {
    return handle(req, res)
  })

  server.listen(PORT, (err) => {
    if (err) throw err
    console.log('> Ready on http://localhost:' + PORT)
  })
})
.catch((ex) => {
  console.error('error', ex)
  process.exit(1)
})