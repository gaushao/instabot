import Layout from '../components/Layout.js'
import Link from 'next/link'

class Index extends React.Component{ 
  constructor(props){
    super(props);
    this.state = {
      userValue : 'open_transformation',
      tagValue : 'merkel'
    }
  }
  getInitialProps = async function(context) {
    return context.query
  }
  render(){
    return (
      <Layout>
        <p>Scrap Instagram User:</p>
        <input type="text" value={this.state.userValue} onChange={(e) => this.setState({userValue: e.target.value})}/>
        <Link href={"/u/" + this.state.userValue}>
          <button>Submit</button>
        </Link>
        <p>Scrap Instagram Tag:</p>
        <input type="text" value={this.state.tagValue} onChange={(e) => this.setState({tagValue: e.target.value})}/>
        <Link href={"/t/" + this.state.tagValue}>
          <button>Submit</button>
        </Link>
      </Layout>
    )
  }
}
export default Index