import Layout from '../components/Layout.js'
import Posts from '../components/InstaPost.js'

const InstaTag = (props) => {
  return (
    <Layout>
    <h1>{props.tag.name}</h1>
    <Posts src={props.tag.posts} />
    </Layout>
  )
}
InstaTag.getInitialProps = async function(context) {
  return context.query
}
export default InstaTag