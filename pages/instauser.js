import Layout from '../components/Layout.js'
import Posts from '../components/InstaPost.js'

const InstaUser = (props) => {
  return (
    <Layout>
        <h1>{props.user.name}</h1>
        <Posts src={props.user.posts} />
    </Layout>
  )
}
InstaUser.getInitialProps = async function(context) {
  return context.query
}
export default InstaUser