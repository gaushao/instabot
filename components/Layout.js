import Header from './Header'
import style from '../styles/main.js'

const Layout = (props) => (
  <div style={style.container}>
    <Header />
    {props.children}
  </div>
)

export default Layout
