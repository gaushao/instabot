import style from '../styles/main.js'

const InstaPost = (props) => {
    const posts = [];
    props.src.forEach((post, index) => {
        posts.push(
        <div key={index} style={style.container}>
            <img src={post.img}/>
            <p>{post.text}</p>
        </div>)
    })
    return (
        <div style={style.container}>
        {posts}
        </div>
    )
}
export default InstaPost
